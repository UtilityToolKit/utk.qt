// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/src/utk/qt/file/isPossibleToReadFile.cpp
//
// Description: Definition of the functions for checking if it is possible to
//              read the specified file.


#include <utk/qt/file/isPossibleToReadFile.hpp>


#include <QDir>
#include <QFileInfo>


bool utk::qt::v1::isPossibleToReadFile (
    const QString i_fileName, const QString i_filePath) {
	return isPossibleToReadFile (
	    QDir (i_filePath).absoluteFilePath (i_fileName));
}


bool utk::qt::isPossibleToReadFile (QString i_fullFileName) {
	QFileInfo fileInfo (i_fullFileName);

	return fileInfo.exists () && fileInfo.isFile () && fileInfo.isReadable ();
}
