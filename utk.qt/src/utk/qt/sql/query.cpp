// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/src/utk/qt/qobject/query.cpp
//
// Description: Definition of the functions for working with QSqlQuery objects.


#include <cmath>

#include <QStringList>

#include <utk/qt/sql/query.hpp>


std::tuple< QString, QStringList > utk::qt::v1::sqlQueryInsertListPlaceholder (
    QString i_query_template,
    unsigned int i_list_element_count,
    QString i_list_placeholder,
    QString i_list_element_prefix) {
	QString formated_query (i_query_template);
	QStringList list_element_placeholders;

	/*
	  Zero-padding is used to eliminate ambbiguity: for example, if there are
	  more then 11 placeholders with "value" prefix, then the placeholders for
	  the 2nd ("value1") and the 11th ("value10") values match the same pattern
	  "value1". This may lead to problems if the user desides to use
	  QString::replace() or this function to further format query template.
	*/
	auto const field_width =
	    (unsigned int)(std::log10 (double(i_list_element_count))) + 1;

	for (unsigned int element_index (0); element_index < i_list_element_count;
	     ++element_index) {
		list_element_placeholders
		    << QString (":%1%2")
		           .arg (i_list_element_prefix)
		           .arg (element_index, field_width, 10, QLatin1Char ('0'));
	}

	formated_query.replace (
	    i_list_placeholder, list_element_placeholders.join (", "));

	return {formated_query, list_element_placeholders};
}


std::tuple< QString, QVariantMap > utk::qt::v1::sqlQueryInsertListPlaceholder (
    QString i_query_template,
    const QMap< QString, QPair< QVariantList, QString > >&
        i_replacement_parameters) {
	QString formated_query (i_query_template);
	QVariantMap bind_values;

	for (const auto list_placeholder : i_replacement_parameters.keys ()) {
		const auto& element_values (
		    i_replacement_parameters[ list_placeholder ].first);
		const auto& element_prefix (
		    i_replacement_parameters[ list_placeholder ].second);

		QStringList element_placeholders;

		std::tie (formated_query, element_placeholders) =
		    sqlQueryInsertListPlaceholder (
		        formated_query,
		        element_values.size (),
		        list_placeholder,
		        element_prefix);

		for (auto element_index = 0;
		     element_index < element_placeholders.size ();
		     ++element_index) {
			bind_values[ element_placeholders[ element_index ] ] =
			    element_values[ element_index ];
		}
	}

	return {formated_query, bind_values};
}


void utk::qt::v1::sqlQueryBindValues (
    const QVariantMap& i_values, QSqlQuery& io_query) {
	for (const auto& placeholder : i_values.keys ()) {
		io_query.bindValue (placeholder, i_values[ placeholder ]);
	}
}


QVariantList
    utk::qt::v1::unpackSqlQueryArrayValue (QString i_packed_array_value) {
	QVariantList unpacked_list;

	if (i_packed_array_value.startsWith ('{') &&
	    i_packed_array_value.endsWith ('}')) {
		i_packed_array_value =
		    i_packed_array_value.mid (1, i_packed_array_value.size () - 2);
	}

	QStringList value_string_list (i_packed_array_value.split (","));

	for (auto& value_string : value_string_list) {
		value_string = value_string.trimmed ();

		if (value_string.startsWith ('"') && value_string.endsWith ('"')) {
			value_string = value_string.mid (1, value_string.size () - 2);
		}

		unpacked_list << value_string;
	}

	return unpacked_list;
}
