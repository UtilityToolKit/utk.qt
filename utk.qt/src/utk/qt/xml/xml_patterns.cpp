// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/src/utk/qt/xml/xml_patterns.cpp
//
// Description: Definition of the functions for working with QXmlPatterns
//              framework.


#include "utk/qt/xml/xml_patterns.hpp"


#include <QTextStream>
#include <QXmlSchemaValidator>

#include <utk/exception/RuntimeError/FormatError.hpp>
#include <utk/exception/RuntimeError/InvalidRuntimeArgument.hpp>


namespace utk_exc = utk::exception;


namespace {
	void loadXmlQuery (
	    QXmlQuery& io_xml_query, QUrl i_xquery_url, QVariantMap i_variables) {
		if (!i_xquery_url.isValid ()) {
			BOOST_THROW_EXCEPTION (
			    utk_exc::InvalidRuntimeArgument ()
			    << utk_exc::InvalidRuntimeArgument::ArgumentInfo (
			           utk_exc::InvalidRuntimeArgument::ArgumentName (
			               "i_xquery_url"),
			           utk_exc::InvalidRuntimeArgument::Description (
			               QString ("Invalid URL: %1")
			                   .arg (i_xquery_url.toString ())
			                   .toStdString ())));
		}

		for (auto variableIterator (i_variables.cbegin ()),
		     variablesEnd (i_variables.cend ());
		     variableIterator != variablesEnd;
		     ++variableIterator) {
			io_xml_query.bindVariable (
			    variableIterator.key (), variableIterator.value ());
		}

		io_xml_query.setQuery (i_xquery_url);

		if (!io_xml_query.isValid ()) {
			BOOST_THROW_EXCEPTION (
			    utk_exc::FormatError () << utk_exc::FormatError::Description (
			        QString ("Invalid XQuery: \"%1\"")
			            .arg (i_xquery_url.toString ())
			            .toStdString ()));
		}
	}
} // namespace


QXmlQuery
    utk::qt::v1::loadXmlQuery (QUrl i_xquery_url, QVariantMap i_variables) {
	QXmlQuery xml_query;

	::loadXmlQuery (xml_query, i_xquery_url, i_variables);

	return xml_query;
}


QXmlQuery utk::qt::v1::loadXmlQuery (
    QUrl i_xquery_url, QIODevice* i_focus, QVariantMap i_variables) {
	QXmlQuery xml_query;

	xml_query.setFocus (i_focus);

	::loadXmlQuery (xml_query, i_xquery_url, i_variables);

	return xml_query;
}


QXmlQuery utk::qt::v1::loadXmlQuery (
    QUrl i_xquery_url, const QString& i_focus, QVariantMap i_variables) {
	QXmlQuery xml_query;

	xml_query.setFocus (i_focus);

	::loadXmlQuery (xml_query, i_xquery_url, i_variables);

	return xml_query;
}


QXmlQuery utk::qt::v1::loadXmlQuery (
    QUrl i_xquery_url, const QUrl& i_focus, QVariantMap i_variables) {
	QXmlQuery xml_query;

	xml_query.setFocus (i_focus);

	::loadXmlQuery (xml_query, i_xquery_url, i_variables);

	return xml_query;
}


QXmlQuery utk::qt::v1::loadXmlQuery (
    QUrl i_xquery_url, const QXmlItem& i_focus, QVariantMap i_variables) {
	QXmlQuery xml_query;

	xml_query.setFocus (i_focus);

	::loadXmlQuery (xml_query, i_xquery_url, i_variables);

	return xml_query;
}


QXmlSchema utk::qt::v1::loadXmlSchema (QUrl i_xsd_url) {
	if (!i_xsd_url.isValid ()) {
		BOOST_THROW_EXCEPTION (
		    utk_exc::InvalidRuntimeArgument ()
		    << utk_exc::InvalidRuntimeArgument::ArgumentInfo (
		           utk_exc::InvalidRuntimeArgument::ArgumentName ("i_xsd_url"),
		           utk_exc::InvalidRuntimeArgument::Description (
		               QString ("Invalid URL: %1")
		                   .arg (i_xsd_url.toString ())
		                   .toStdString ())));
	}

	QXmlSchema schema;

	if (!schema.load (i_xsd_url)) {
		BOOST_THROW_EXCEPTION (
		    utk_exc::FormatError () << utk_exc::FormatError::Description (
		        QString ("Invalid schema: \"%1\"")
		            .arg (i_xsd_url.toString ())
		            .toStdString ()));
	}

	return schema;
}


void utk::qt::v1::validateXmlDomNode (
    const QDomNode& i_xml_dom_node,
    QUrl i_xsd_url,
    QUrl i_xml_url,
    QAbstractMessageHandler* i_message_handler) {
	validateXmlDomNode (
	    i_xml_dom_node,
	    loadXmlSchema (i_xsd_url),
	    i_xml_url,
	    i_message_handler);
}


void utk::qt::v1::validateXmlDomNode (
    const QDomNode& i_xml_dom_node,
    const QXmlSchema& i_xml_schema,
    QUrl i_xml_url,
    QAbstractMessageHandler* i_message_handler) {
	QString xml;
	{
		QTextStream xml_stream{&xml};

		xml_stream.setCodec ("utf-8");

		i_xml_dom_node.save (xml_stream, 2);
	}

	QXmlSchemaValidator schemaValidator (i_xml_schema);

	if (i_message_handler) {
		schemaValidator.setMessageHandler (i_message_handler);
	}

	if (!schemaValidator.validate (xml.toUtf8 (), i_xml_url)) {
		BOOST_THROW_EXCEPTION (
		    utk_exc::FormatError () << utk_exc::FormatError::Description (
		        QString ("Invalid XML: \"%1\"")
		            .arg (i_xml_url.toString ())
		            .toStdString ()));
	}
}


void utk::qt::v1::validateXml (
    QString i_xml,
    QUrl i_xsd_url,
    QUrl i_xml_url,
    QAbstractMessageHandler* i_message_handler) {
	utk::qt::v1::validateXml (
	    i_xml, loadXmlSchema (i_xsd_url), i_xml_url, i_message_handler);
}


void utk::qt::v1::validateXml (
    QString i_xml,
    const QXmlSchema& i_xml_schema,
    QUrl i_xml_url,
    QAbstractMessageHandler* i_message_handler) {
	QXmlSchemaValidator schemaValidator (i_xml_schema);

	if (i_message_handler) {
		schemaValidator.setMessageHandler (i_message_handler);
	}

	if (!schemaValidator.validate (i_xml.toUtf8 (), i_xml_url)) {
		BOOST_THROW_EXCEPTION (
		    utk_exc::FormatError () << utk_exc::FormatError::Description (
		        QString ("Invalid XML: \"%1\"")
		            .arg (i_xml_url.toString ())
		            .toStdString ()));
	}
}


void utk::qt::v1::validateXmlFile (
    QUrl i_xmlFileUrl,
    QUrl i_xsd_url,
    QAbstractMessageHandler* i_message_handler) {
	utk::qt::v1::validateXmlFile (
	    i_xmlFileUrl, loadXmlSchema (i_xsd_url), i_message_handler);
}


void utk::qt::v1::validateXmlFile (
    QUrl i_xmlFileUrl,
    const QXmlSchema& i_xml_schema,
    QAbstractMessageHandler* i_message_handler) {
	if (!i_xmlFileUrl.isValid ()) {
		BOOST_THROW_EXCEPTION (
		    utk_exc::InvalidRuntimeArgument ()
		    << utk_exc::InvalidRuntimeArgument::ArgumentInfo (
		           utk_exc::InvalidRuntimeArgument::ArgumentName (
		               "i_xmlFileUrl"),
		           utk_exc::InvalidRuntimeArgument::Description (
		               QString ("Invalid file URL: \"%1\".")
		                   .arg (i_xmlFileUrl.toString ())
		                   .toStdString ())));
	}

	QXmlSchemaValidator schemaValidator (i_xml_schema);

	if (i_message_handler) {
		schemaValidator.setMessageHandler (i_message_handler);
	}

	if (!schemaValidator.validate (i_xmlFileUrl)) {
		BOOST_THROW_EXCEPTION (
		    utk_exc::FormatError () << utk_exc::FormatError::Description (
		        QString ("Invalid file: \"%1\"")
		            .arg (i_xmlFileUrl.toString ())
		            .toStdString ()));
	}
}
