// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/include/utk/qt/xml/xml_dom.hpp
//
// Description: Declaration of the functions for working with QDomNode objects.


#ifndef INCLUDE_UTK_QT_XML_XML_DOM_HPP
#define INCLUDE_UTK_QT_XML_XML_DOM_HPP


#include <QDomNode>
#include <QString>

#include "utk/qt/utk_qt_export.h"


namespace utk {
	namespace qt {
		inline namespace v1 {
			UTK_QT_EXPORT bool
			    readXmlFile (QString i_fullFileName, QDomNode& o_content);

			UTK_QT_EXPORT bool writeXmlFile (
			    QString i_fullFileName,
			    QDomNode i_content,
			    QString i_codec = "UTF-8");
		}
	}
}


#endif /* INCLUDE_UTK_QT_XML_XML_DOM_HPP */
