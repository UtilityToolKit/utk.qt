// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/include/utk/qt/qobject/record.hpp
//
// Description: Declaration of the functions for working with QSqlRecord objects.


#ifndef INCLUDE_UTK_QT_SQL_RECORD_HPP
#define INCLUDE_UTK_QT_SQL_RECORD_HPP


#include <QSqlRecord>

#include "utk/qt/utk_qt_export.h"


namespace utk {
	namespace qt {
		inline namespace v1 {
			UTK_QT_EXPORT bool
			    QueryRecordIsNull (const QSqlRecord& i_record);
		}
	} // namespace qt
} // namespace utk


#endif /* INCLUDE_UTK_QT_SQL_RECORD_HPP */
