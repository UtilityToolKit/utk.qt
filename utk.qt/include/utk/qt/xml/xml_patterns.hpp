// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/include/utk/qt/xml/xml_patterns.hpp
//
// Description: Declaration of the functions for working with QXmlPatterns
//              framework.


#ifndef INCLUDE_UTK_QT_XML_XML_PATTERNS_HPP
#define INCLUDE_UTK_QT_XML_XML_PATTERNS_HPP


#include <QAbstractMessageHandler>
#include <QDomNode>
#include <QIODevice>
#include <QString>
#include <QUrl>
#include <QVariantMap>
#include <QXmlQuery>
#include <QXmlSchema>

#include "utk/qt/utk_qt_export.h"


namespace utk {
	namespace qt {
		inline namespace v1 {
			UTK_QT_EXPORT QXmlQuery loadXmlQuery (
			    QUrl i_xquery_url, QVariantMap i_variables = QVariantMap ());

			UTK_QT_EXPORT QXmlQuery loadXmlQuery (
			    QUrl i_xquery_url,
			    QIODevice* i_focus,
			    QVariantMap i_variables = QVariantMap ());

			UTK_QT_EXPORT QXmlQuery loadXmlQuery (
			    QUrl i_xquery_url,
			    const QString& i_focus,
			    QVariantMap i_variables = QVariantMap ());

			UTK_QT_EXPORT QXmlQuery loadXmlQuery (
			    QUrl i_xquery_url,
			    const QUrl& i_focus,
			    QVariantMap i_variables = QVariantMap ());

			UTK_QT_EXPORT QXmlQuery loadXmlQuery (
			    QUrl i_xquery_url,
			    const QXmlItem& i_focus,
			    QVariantMap i_variables = QVariantMap ());


			UTK_QT_EXPORT QXmlSchema loadXmlSchema (QUrl i_xsd_url);


			UTK_QT_EXPORT void validateXmlDomNode (
			    const QDomNode& i_xml_dom_node,
			    QUrl i_xsd_url,
			    QUrl i_xml_url = QUrl{},
			    QAbstractMessageHandler* i_message_handler = nullptr);

			UTK_QT_EXPORT void validateXmlDomNode (
			    const QDomNode& i_xml_dom_node,
			    const QXmlSchema& i_xml_schema,
			    QUrl i_xml_url = QUrl{},
			    QAbstractMessageHandler* i_message_handler = nullptr);


			UTK_QT_EXPORT void validateXml (
			    QString i_xml,
			    QUrl i_xsd_url,
			    QUrl i_xml_url = QUrl{},
			    QAbstractMessageHandler* i_message_handler = nullptr);

			UTK_QT_EXPORT void validateXml (
			    QString i_xml,
			    const QXmlSchema& i_xml_schema,
			    QUrl i_xml_url = QUrl{},
			    QAbstractMessageHandler* i_message_handler = nullptr);


			UTK_QT_EXPORT void validateXmlFile (
			    QUrl i_xmlFileUrl,
			    QUrl i_xsd_url,
			    QAbstractMessageHandler* i_message_handler = nullptr);

			UTK_QT_EXPORT void validateXmlFile (
			    QUrl i_xmlFileUrl,
			    const QXmlSchema& i_xml_schema,
			    QAbstractMessageHandler* i_message_handler = nullptr);
		} // namespace v1
	}     // namespace qt
} // namespace utk


#endif /* INCLUDE_UTK_QT_XML_XML_PATTERNS_HPP */
