include (FetchContent)


set (utk_factory_INSTALL_DEVEL FALSE CACHE BOOL "")


FetchContent_Declare (
  utk_factory
  GIT_REPOSITORY https://gitlab.com/UtilityToolKit/utk.factory.git
  GIT_TAG        v2.2.1
  GIT_PROGRESS   TRUE
  GIT_SHALLOW    TRUE
  )

FetchContent_GetProperties (utk_factory)

if (NOT utk_factory_POPULATED)
  FetchContent_Populate (utk_factory)

  add_subdirectory (${utk_factory_SOURCE_DIR} ${utk_factory_BINARY_DIR})
endif()
