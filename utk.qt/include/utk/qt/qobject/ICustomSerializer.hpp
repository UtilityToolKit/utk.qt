// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/include/utk/qt/qobject/ICustomSerializer.hpp
//
// Description: Declaration of the ICustomSerializer class member functions.


#ifndef INCLUDE_UTK_QT_QOBJECT_ICUSTOMSERIALIZER_HPP
#define INCLUDE_UTK_QT_QOBJECT_ICUSTOMSERIALIZER_HPP


#include <QDomNode>
#include <QString>
#include <QVariant>

#include <utk/factory/v1/Factory.hpp>

#include "utk/qt/utk_qt_export.h"


class QObject;

namespace utk {
	namespace qt {
		inline namespace v1 {
			/**
			  @biref Interface for user defuned serializers.
			*/
			class UTK_QT_EXPORT ICustomSerializer
			    : public utk::factory::IProducible {
			public:
				typedef std::shared_ptr< ICustomSerializer > SharedPtr;

				static SharedPtr createObject ();

				static QString typeId ();

				virtual ~ICustomSerializer ();

				/**
				  @brief Performs seralization of given value.

				  @details The value is expected to be stored in QVariant. The
				  reason for that is that this class is supposed to be used to
				  serialize objects properties, values of which are retreived as
				  QVariant is requested with property() function.

				  @param [in] i_value Property value to serialize.

				  @returns QDomNode with serialized value.
				*/
				virtual QDomNode serialize (QVariant i_value) = 0;
			};

			typedef utk::factory::Factory< utk::qt::ICustomSerializer >
				CustomSerializerFactory;
		}
	}
}


namespace utk {
	namespace factory {
		inline namespace v1 {

			UTK_QT_EXTERN template class UTK_QT_EXPORT
				utk::factory::Factory< utk::qt::ICustomSerializer >;
		}
	}
}


#endif /* INCLUDE_UTK_QT_QOBJECT_ICUSTOMSERIALIZER_HPP */
