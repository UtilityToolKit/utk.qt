// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/src/utk/qt/widget/ComboBoxExt.cpp
//
// Description: Definition of the ComboBoxExt interface class.


#include "ComboBoxExtImpl.hpp"

#include <utk/pimpl/Pimpl_impl.hpp>

#include <algorithm>
#include <iterator>

#include <utk/exception/LogicError/InvalidLogicArgument.hpp>


const char* utk::qt::v1::ComboBoxExt::AttachedPropertyName () {
	return "utk_qt_combo_box_ext";
}


utk::qt::v1::ComboBoxExt*
    utk::qt::v1::ComboBoxExt::ExtractAttached (const QComboBox* i_combo_box) {
	if (!i_combo_box) {
		BOOST_THROW_EXCEPTION (
		    utk::exception::InvalidLogicArgument ()
		    << utk::exception::InvalidLogicArgument::ArgumentName (
		           "i_combo_box")
		    << utk::exception::InvalidLogicArgument::Description (
		           "Cannot extract attached ComboBoxExtfrom nullptr"));
	}

	auto const attached_property_value =
	    i_combo_box->property (AttachedPropertyName ());

	auto attached_combo_box_ext = attached_property_value.isValid ()
	    ? attached_property_value.value< ComboBoxExt* > ()
	    : nullptr;

	return attached_combo_box_ext;
}


utk::qt::v1::ComboBoxExt::ComboBoxExt (QComboBox* i_combo_box)
    : QObject (i_combo_box)
    , impl_{this} {
	if (i_combo_box) {
		AttachToComboBox (i_combo_box);
	}
}


utk::qt::v1::ComboBoxExt::~ComboBoxExt () = default;


void utk::qt::v1::ComboBoxExt::EnableNotification () {
	if (impl_->combo_box) {
		impl_->current_index_changed_connection = connect (
		    impl_->combo_box,
		    QOverload< int >::of (&QComboBox::currentIndexChanged),
		    this,
		    [this](int i_new_index) {
			    impl_->OnCurrentItemIndexChanged (i_new_index);
		    });
	}

	impl_->notification_enabled = true;
}


void utk::qt::v1::ComboBoxExt::DisableNotification () {
	disconnect (impl_->current_index_changed_connection);

	impl_->current_index_changed_connection = QMetaObject::Connection{};
	impl_->notification_enabled = false;
}


void utk::qt::v1::ComboBoxExt::AddItemsWithData (
    const QList< QPair< QString, QVariant > >& i_items_with_data) {
	impl_->AddItemsToComboBox (i_items_with_data);
}


void utk::qt::v1::ComboBoxExt::SetItemsWithData (
    const QList< QPair< QString, QVariant > >& i_items_with_data) {
	disconnect (impl_->current_index_changed_connection);

	impl_->items_with_data.clear ();

	if (impl_->combo_box) {
		impl_->combo_box->clear ();
	}

	impl_->AddItemsToComboBox (i_items_with_data);
}


void utk::qt::v1::ComboBoxExt::SetComboBox (QComboBox* i_combo_box) {
	disconnect (impl_->destroyed_connection);
	disconnect (impl_->current_index_changed_connection);

	impl_->combo_box = i_combo_box;

	impl_->AddItemsToComboBox (impl_->items_with_data);

	if (i_combo_box) {
		impl_->destroyed_connection =
		    connect (impl_->combo_box, &QComboBox::destroyed, this, [this]() {
			    impl_->combo_box = nullptr;
		    });
	}
}


void utk::qt::v1::ComboBoxExt::AttachToComboBox (QComboBox* i_combo_box) {
	if (!i_combo_box) {
		BOOST_THROW_EXCEPTION (
		    utk::exception::InvalidLogicArgument ()
		    << utk::exception::InvalidLogicArgument::ArgumentName (
		           "i_combo_box")
		    << utk::exception::InvalidLogicArgument::Description (
		           "Cannot attach to nullptr"));
	}

	i_combo_box->setProperty (
	    AttachedPropertyName (), QVariant::fromValue (this));

	setParent (i_combo_box);
}
