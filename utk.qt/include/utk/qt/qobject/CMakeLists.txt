# Copyright 2015-2021 Utility Tool Kit Open Source Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
# File name: utk.qt/utk.qt/include/utk/qt/qobject/CMakeLists.txt
#
# Description: Utility Tool Kit Qt Library dependencies.


set (HEADERS
  ICustomDeserializer.hpp
  ICustomSerializer.hpp
  QObjectSerialization.hpp
  QRectDeserializer.hpp
  QRectSerializer.hpp
  QStringListDeserializer.hpp
  QStringListSerializer.hpp
  QVector3DDeserializer.hpp
  QVector3DSerializer.hpp
  )

file(RELATIVE_PATH
  PREFIX
  ${PROJECT_SOURCE_DIR}
  ${CMAKE_CURRENT_LIST_DIR})


foreach (HEADER IN LISTS HEADERS)
  target_sources (${${PROJECT_NAME}_TARGET_NAME}
    PRIVATE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/${HEADER}>
    $<INSTALL_INTERFACE:${PREFIX}/${HEADER}>)
endforeach (HEADER IN HEADERS)
