include (FetchContent)


set (utk_INSTALL_DEVEL FALSE CACHE BOOL "")


FetchContent_Declare (
  utk
  GIT_REPOSITORY https://gitlab.com/UtilityToolKit/utk.git
  GIT_TAG        v0.2.1
  GIT_PROGRESS   TRUE
  GIT_SHALLOW    TRUE
  )

FetchContent_GetProperties (utk)

if (NOT utk_POPULATED)
  FetchContent_Populate (utk)

  add_subdirectory (${utk_SOURCE_DIR} ${utk_BINARY_DIR})
endif()
