// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/include/utk/qt/qobject/QObjectSerialization.hpp
//
// Description: Declaration of the functions used to implement QObject
//              meta-information processing and QObject serialisation.


#ifndef INCLUDE_UTK_QT_QOBJECT_QOBJECTSERIALIZATION_HPP
#define INCLUDE_UTK_QT_QOBJECT_QOBJECTSERIALIZATION_HPP


#include <QDomNode>
#include <QSet>
#include <QString>
#include <QStringList>

#include "utk/qt/utk_qt_export.h"


class QObject;


namespace utk {
	namespace qt {
		inline namespace v1 {
			/**
			   @brief Possible sources for tag name of XML DOM node used for
			   object serialization.
			*/
			enum class TagNameSource {
				/**
			      @brief Use object type name as tag name.
			    */
				TAG_TYPE_NAME,
				/**
			      @brief Use object name as tag name.
			    */
				TAG_OBJECT_NAME,
				/**
			      @brief Use user provided tag name.
			    */
				TAG_USER_DEFINED
			};

			/**
			   @brief A helper function to get object's properties names set.

			   @param [in] i_object Object to get its properties set.

			   @returns Set of object's properties names.
			*/
			UTK_QT_EXPORT QSet< QString >
			    objectPropertiesSet (QObject* i_object);

			/**
			   @brief Perform serialization of object properties to XML DOM.

			   @details Properties are saved in 2 different ways:

			   - those properties that have type that can be converted in
			   QString are stored as attributes;

			   - those properties that have type that can not be converted in
			   QString are stored in child nodes with respective name if
			   serializer for type is provided.

			   @param [in] i_object Object to serialize its properties.

			   @param [in] i_tagNameSource Tag name source to use to form XML
			   DOM node tag name.

			   @param [in] i_tagName Tag name for XML DOM node which will store
			   serialized object properties.  Required id i_tagNameSource =
			   TAG_USER_DEFINED.

			   @param [in] i_propertiesNames A list of properties to serialize.
			   If the list is empty, then all properties are serialized.

			   @returns XML DOM node with serialized object properties. Node
			   will be null on failure.
			*/
			UTK_QT_EXPORT QDomNode serialize (
			    QObject* i_object,
			    TagNameSource i_tagNameSource,
			    QString i_tagName = QString (),
			    QStringList i_propertiesNames = QStringList ());

			/**
			   @brief Perform deserialization of object properties from XML DOM.

			   @details The object must be created before call to this function.
			   If any of properties is not stored in XML DOM it will not be
			   changed.

			   The structure of XML DOM should be the same as created by
			   serialize() function.

			   Properties, stored in "property" nodes will be deserialized only
			   if there is registered deserializer for respective type.

			   @param [out] o_object Object to set its properties according to
			   contents of XML DOM node.

			   @param [in] i_serialized XML DOM node with serialized properties
			   of an object.
			*/
			UTK_QT_EXPORT void
			    deserialize (QObject* o_object, QDomNode i_serialized);

			/**
			   @brief Searches for XML DOM node which contains serialized
			   properties of an object.

			   @details Search is performed recursively beginning from the given
			   node itself.

			   The first node that matches search criteria is returned.

			   @param [in] i_serialisedObjects XML DOM node with serialized
			   object somewhere inside it.

			   @param [in] i_object Serialized properties of this object are
			   being searched.

			   @param [in] tagNameSource_t Tag name source to use to form XML
			   DOM node tag name to find.

			   @param [in] i_tagName Tag name to find. Required if
			   i_tagNameSource = TAG_USER_DEFINED.
			*/
			UTK_QT_EXPORT QDomNode getSerializedObjectNode (
			    QDomNode i_serialisedObjects,
			    QObject* i_object,
			    TagNameSource i_tagNameSource,
			    QString i_tagName = QString ());
		}
	}
}


#endif /* INCLUDE_UTK_QT_QOBJECT_QOBJECTSERIALIZATION_HPP */
