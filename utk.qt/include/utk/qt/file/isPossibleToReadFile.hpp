// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/include/utk/qt/file/isPossibleToReadFile.hpp
//
// Description: Declaration of the functions for checking if it is possible to
//              read the specified file.


#ifndef INCLUDE_UTK_QT_FILE_ISPOSSIBLETOREADFILE_HPP
#define INCLUDE_UTK_QT_FILE_ISPOSSIBLETOREADFILE_HPP


#include <QString>

#include "utk/qt/utk_qt_export.h"


namespace utk {
	namespace qt {
		inline namespace v1 {
			UTK_QT_EXPORT bool isPossibleToReadFile (
			    const QString i_fileName, const QString i_filePath);

			UTK_QT_EXPORT bool isPossibleToReadFile (QString i_fullFileName);
		}
	}
}


#endif /* INCLUDE_UTK_QT_FILE_ISPOSSIBLETOREADFILE_HPP */
