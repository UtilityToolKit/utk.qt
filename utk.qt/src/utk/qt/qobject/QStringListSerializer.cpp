// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/src/utk/qt/qobject/QStringListSerializer.cpp
//
// Description: Definition of the QStringListSerializer class.


#include "utk/qt/qobject/QStringListSerializer.hpp"


#include <QStringList>


utk::factory::AddProduction<
    utk::qt::v1::ICustomSerializer,
    utk::qt::v1::QStringListSerializer >
    addQStringListSerializer;


utk::qt::v1::ICustomSerializer::SharedPtr
    utk::qt::v1::QStringListSerializer::createObject () {
	return std::make_shared< QStringListSerializer > ();
}


QString utk::qt::v1::QStringListSerializer::typeId () {
	static QString s_typeId (QMetaType::typeName (QMetaType::QStringList));

	return s_typeId;
}


QDomNode utk::qt::v1::QStringListSerializer::serialize (QVariant i_value) {
	QDomDocument serializedDocument;

	QDomElement serializedElement (
	    serializedDocument.createElement (typeId ()));

	serializedDocument.appendChild (serializedElement);

	QStringList stringList (i_value.toStringList ());

	foreach (QString string, stringList) {
		QDomElement stringElement (serializedDocument.createElement ("string"));

		stringElement.setAttribute ("text", string);

		serializedElement.appendChild (stringElement);
	}

	return QDomNode (serializedDocument);
}
