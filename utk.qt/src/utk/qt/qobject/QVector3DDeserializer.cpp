// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/src/utk/qt/qobject/QVector3DDeserializer.cpp
//
// Description: Definition of the QVector3DDeserializer class.


#include "utk/qt/qobject/QVector3DDeserializer.hpp"


#include <QVector3D>


utk::factory::AddProduction<
    utk::qt::v1::ICustomDeserializer,
    utk::qt::v1::QVector3DDeserializer >
    addQVector3DDeserializer;


utk::qt::v1::ICustomDeserializer::SharedPtr
    utk::qt::v1::QVector3DDeserializer::createObject () {
	return std::make_shared< QVector3DDeserializer > ();
}


QString utk::qt::v1::QVector3DDeserializer::typeId () {
	static QString s_typeId (QMetaType::typeName (QMetaType::QVector3D));

	return s_typeId;
}


QVariant utk::qt::v1::QVector3DDeserializer::deserialize (
    QDomNode i_serializedValue) {
	QVariant deserializedValue;

	QDomElement serializedElement (i_serializedValue.toElement ());

	if (!serializedElement.isNull () &&
	    (typeId () == serializedElement.tagName ())) {
		deserializedValue = QVector3D (
		    serializedElement.attribute ("x").toFloat (),
		    serializedElement.attribute ("y").toFloat (),
		    serializedElement.attribute ("z").toFloat ());
	}

	return deserializedValue;
}
