// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/src/utk/qt/widget/ComboBoxExtImpl.hpp
//
// Description: Declaration of the ComboBoxExt PIMPL class.


#ifndef SRC_UTK_QT_WIDGET_COMBOBOXEXTIMPL_HPP
#define SRC_UTK_QT_WIDGET_COMBOBOXEXTIMPL_HPP


#include <QMetaObject>
#include <QObject>


#include "utk/qt/widget/ComboBoxExt.hpp"


namespace utk {
	namespace qt {
		inline namespace v1 {
			class UTK_QT_NO_EXPORT ComboBoxExt::Impl {
			public:
				Impl (ComboBoxExt* i_owner);

				void AddItemsToComboBox (
				    const QList< QPair< QString, QVariant > >&
				        i_items_with_data);


			public Q_SLOTS:
				/**
				   @brief Обработка изменения текущего выбранного элемента.

				   @details В случае, если данные ассоциированные с текущим
				   выбранным элементом валидны, испускается сигнал
				   currentItemData().

				   @param [in] i_newIndex Индекс нового текущего выбранного
				   элемента списка.
				*/
				void OnCurrentItemIndexChanged (int i_new_index);


			public:
				QComboBox* combo_box = nullptr;

				QMetaObject::Connection current_index_changed_connection;
				bool notification_enabled = false;

				QMetaObject::Connection destroyed_connection;

				QList< QPair< QString, QVariant > > items_with_data;


			private:
				ComboBoxExt* owner_ = nullptr;
			};
		} // namespace v1
	}     // namespace qt
} // namespace utk


#endif /* SRC_UTK_QT_WIDGET_COMBOBOXEXTIMPL_HPP */
