// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/include/utk/qt/qobject/query.hpp
//
// Description: Declaration of the functions for working with QSqlQuery objects.


#ifndef INCLUDE_UTK_QT_SQL_QUERY_HPP
#define INCLUDE_UTK_QT_SQL_QUERY_HPP


#include <tuple>

#include <QSqlQuery>
#include <QString>
#include <QVariant>

#include "utk/qt/utk_qt_export.h"


namespace utk {
	namespace qt {
		inline namespace v1 {
			UTK_QT_EXPORT std::tuple< QString, QStringList >
			    sqlQueryInsertListPlaceholder (
			        QString i_query_template,
			        unsigned int i_list_element_count,
			        QString i_list_placeholder,
			        QString i_list_element_prefix);

			UTK_QT_EXPORT std::tuple< QString, QVariantMap >
			    sqlQueryInsertListPlaceholder (
			        QString i_query_template,
			        const QMap< QString, QPair< QVariantList, QString > >&
			            i_replacement_parameters);

			UTK_QT_EXPORT void sqlQueryBindValues (
			    const QVariantMap& i_values, QSqlQuery& io_query);

			UTK_QT_EXPORT QVariantList
			    unpackSqlQueryArrayValue (QString i_packed_list_value);
		}
	}
}


#endif /* INCLUDE_UTK_QT_SQL_QUERY_HPP */
