// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/src/utk/qt/qobject/QVector3DSerializer.cpp
//
// Description: Definition of the QVector3DSerializer class.


#include <QVector3D>


#include "utk/qt/qobject/QVector3DSerializer.hpp"


utk::factory::AddProduction<
    utk::qt::v1::ICustomSerializer,
    utk::qt::v1::QVector3DSerializer >
    addQVector3DSerializer;


utk::qt::v1::ICustomSerializer::SharedPtr
    utk::qt::v1::QVector3DSerializer::createObject () {
	return std::make_shared< QVector3DSerializer > ();
}


QString utk::qt::v1::QVector3DSerializer::typeId () {
	static QString s_typeId (QMetaType::typeName (QMetaType::QVector3D));

	return s_typeId;
}


QDomNode utk::qt::v1::QVector3DSerializer::serialize (QVariant i_value) {
	QDomDocument serializedDocument;

	if (i_value.canConvert< QVector3D > ()) {
		QDomElement serializedElement (
		    serializedDocument.createElement (typeId ()));

		serializedDocument.appendChild (serializedElement);

		QVector3D vactor3D (i_value.value< QVector3D > ());

		serializedElement.setAttribute ("x", QString::number (vactor3D.x ()));
		serializedElement.setAttribute ("y", QString::number (vactor3D.y ()));
		serializedElement.setAttribute ("z", QString::number (vactor3D.z ()));
	}

	return QDomNode (serializedDocument);
}
