// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/src/utk/qt/file/isPossibleToWriteFile.cpp
//
// Description: Definition of the functions for checking if it is possible to
//              write the specified file.


#include <utk/qt/file/isPossibleToWriteFile.hpp>


#include <QDir>
#include <QFile>
#include <QFileInfo>


bool utk::qt::v1::isPossibleToWriteFile (
    const QString i_fileName,
    const QString i_filePath,
    bool i_forceRewrite,
    bool i_createPath) {
	bool isPossibleToWriteFile (false);

	QFileInfo targetDirInfo (i_filePath);

	if (!targetDirInfo.exists () ||
	    (targetDirInfo.exists () && targetDirInfo.isDir ())) {
		QDir targetDir (targetDirInfo.absoluteFilePath ());

		if (!targetDirInfo.exists () && i_createPath) {
			targetDir.mkpath (targetDirInfo.absoluteFilePath ());
		}

		if (targetDirInfo.exists ()) {
			QString targetFileName (targetDir.absoluteFilePath (i_fileName));
			QFileInfo targetFileInfo (targetFileName);

			if (i_forceRewrite) {
				if (!targetFileInfo.exists () ||
				    (targetFileInfo.exists () &&
				     targetFileInfo.isWritable ())) {
					isPossibleToWriteFile = true;
				}
			}
			else {
				if (!targetFileInfo.exists ()) {
					isPossibleToWriteFile = true;
				}
			}
		}
	}

	return isPossibleToWriteFile;
}


bool utk::qt::v1::isPossibleToWriteFile (
    const QString i_fullFileName, bool i_forceRewrite, bool i_createPath) {
	QFileInfo targetInfo (i_fullFileName);

	return isPossibleToWriteFile (
	    targetInfo.fileName (),
	    targetInfo.absolutePath (),
	    i_forceRewrite,
	    i_createPath);
}
