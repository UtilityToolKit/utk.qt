// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/include/utk/qt/qobject/ICustomDeserializer.hpp
//
// Description: Declaration of the ICustomDeserializer class member functions.


#ifndef INCLUDE_UTK_QT_QOBJECT_ICUSTOMDESERIALIZER_HPP
#define INCLUDE_UTK_QT_QOBJECT_ICUSTOMDESERIALIZER_HPP


#include <QDomNode>
#include <QString>
#include <QVariant>

#include <utk/factory/v1/Factory.hpp>

#include "utk/qt/utk_qt_export.h"


class QObject;


namespace utk {
	namespace qt {
		inline namespace v1 {
			/**
			  @biref Interface for user defuned deserializers.
			*/
			class UTK_QT_EXPORT ICustomDeserializer
			    : public utk::factory::IProducible {
			public:
				typedef std::shared_ptr< ICustomDeserializer > SharedPtr;

				static SharedPtr createObject ();

				static QString typeId ();

				virtual ~ICustomDeserializer ();

				/**
				  @brief Performs deseralization of given serialized value.

				  @details The serialized value is expected to be stored in XMl
				  DOM node. Result is returned as QVariant because this class is
				  supposed to be used to deserialize objects properties, values
				  of which are set with setProperty() function expecting
				  QVariant as input.
				*/
				virtual QVariant deserialize (QDomNode i_serializedValue) = 0;
			};

			typedef utk::factory::Factory< utk::qt::ICustomDeserializer >
				CustomDeserializerFactory;
		}
	}
}


UTK_QT_EXTERN template class UTK_QT_EXPORT
	utk::factory::Factory< utk::qt::ICustomDeserializer >;


#endif /* INCLUDE_UTK_QT_QOBJECT_ICUSTOMDESERIALIZER_HPP */
