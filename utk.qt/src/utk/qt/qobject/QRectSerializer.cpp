// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/src/utk/qt/qobject/QRectSerializer.cpp
//
// Description: Definition of the QRectSerializer class.


#include "utk/qt/qobject/QRectSerializer.hpp"


#include <QRect>
#include <QRectF>


utk::factory::AddProduction<
    utk::qt::v1::ICustomSerializer,
    utk::qt::v1::QRectFSerializer >
    addQRectFSerializer;
utk::factory::AddProduction<
    utk::qt::v1::ICustomSerializer,
    utk::qt::v1::QRectSerializer >
    addQRectSerializer;


utk::qt::v1::ICustomSerializer::SharedPtr
    utk::qt::v1::QRectFSerializer::createObject () {
	return std::make_shared< QRectFSerializer > ();
}


QString utk::qt::v1::QRectFSerializer::typeId () {
	static QString s_typeId (QMetaType::typeName (QMetaType::QRectF));

	return s_typeId;
}


QDomNode utk::qt::v1::QRectFSerializer::serialize (QVariant i_value) {
	QDomDocument serializedDocument;

	QDomElement serializedElement (
	    serializedDocument.createElement (typeId ()));

	serializedDocument.appendChild (serializedElement);

	QRectF rect (i_value.toRectF ());

	serializedElement.setAttribute ("height", QString::number (rect.height ()));
	serializedElement.setAttribute ("width", QString::number (rect.width ()));
	serializedElement.setAttribute ("x", QString::number (rect.x ()));
	serializedElement.setAttribute ("y", QString::number (rect.y ()));

	return QDomNode (serializedDocument);
}


utk::qt::v1::ICustomSerializer::SharedPtr
    utk::qt::v1::QRectSerializer::createObject () {
	return std::make_shared< QRectSerializer > ();
}


QString utk::qt::v1::QRectSerializer::typeId () {
	static QString s_typeId (QMetaType::typeName (QMetaType::QRect));

	return s_typeId;
}


QDomNode utk::qt::v1::QRectSerializer::serialize (QVariant i_value) {
	QDomElement serializedElement (QRectFSerializer ()
	                                   .serialize (i_value)
	                                   .toDocument ()
	                                   .documentElement ());

	serializedElement.setTagName ("QRect");

	return QDomNode (serializedElement);
}
