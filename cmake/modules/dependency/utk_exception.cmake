include (FetchContent)


set (utk_exception_INSTALL_DEVEL FALSE CACHE BOOL "")


FetchContent_Declare (
  utk_exception
  GIT_REPOSITORY https://gitlab.com/UtilityToolKit/utk.exception.git
  GIT_TAG        v0.6.0
  GIT_PROGRESS   TRUE
  GIT_SHALLOW    TRUE
  )

FetchContent_GetProperties (utk_exception)

if (NOT utk_exception_POPULATED)
  FetchContent_Populate (utk_exception)

  add_subdirectory (${utk_exception_SOURCE_DIR} ${utk_exception_BINARY_DIR})
endif()

set_target_properties (utk_exception
  PROPERTIES
  FOLDER "Dependencies/utk"
  )
