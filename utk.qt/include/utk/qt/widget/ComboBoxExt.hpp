// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/include/utk/qt/widget/ComboBoxExt.hpp
//
// Description: Declaration of the ComboBoxExt interface class.


#ifndef INCLUDE_UTK_QT_WIDGET_COMBOBOXEXT_HPP
#define INCLUDE_UTK_QT_WIDGET_COMBOBOXEXT_HPP


#include <QComboBox>
#include <QObject>

#include <utk/pimpl/Pimpl.hpp>

#include "utk/qt/utk_qt_export.h"


namespace utk {
	namespace qt {
		inline namespace v1 {
			/**
			   @brief Виджет выпадающего списка, рассылающий значение,
			   ассоциированное с текущим выбранным элементом списка.

			   @details Рассылка значения производится при помощи сигнала. При
			   изменении текущего выбранного варианта из списка испускается
			   сигнал, имеющий в качестве передаваемого значения то значение,
			   которое ассоциировано с выбранным элементом списка. В случае,
			   если ассоциированное значение не валидно, сингал не будет
			   испущен.

			   Для включания рассылки значений необходимо вызвать метод
			   enableNotification().  Такая необходимость обусловлена тем, что
			   при добавлении элементов в пустой виджет сигнал об изменении
			   текущего индекса также испускается, что может привести к ложной
			   отправке оповещения.
			*/
			class UTK_QT_EXPORT ComboBoxExt : public QObject {
				Q_OBJECT

			public:
				static const char* AttachedPropertyName ();
				static ComboBoxExt*
				    ExtractAttached (const QComboBox* i_combo_box);


				/**
				   @brief Конструктор.

				   @param [in] i_parent Родительский объект в иерархии QObject.
				*/
				explicit ComboBoxExt (QComboBox* i_combo_box = nullptr);

				~ComboBoxExt ();

				/**
				   @brief Включение оповещения об изменении данных.

				   @details Включает испускание сигнала, используемого для
				   передачи данных, ассоциированных с текущим выбранным
				   элементом, при изменении текущего выбранного элемента.
				*/
				void EnableNotification ();

				/**
				   @brief Выключение оповещения об изменении данных.

				   @details Выключает испускание сигнала, используемого для
				   передачи данных, ассоциированных с текущим выбранным
				   элементом, при изменении текущего выбранного элемента.
				*/
				void DisableNotification ();

				/**
				   @brief Заполнение списка выбора элементами с ассоциированными
				   данными.

				   @details Выполняет добавление всех элементов с
				   ассоциированными данными в список элементов.  На время
				   добавления выключается рассылка значений.

				   @param [in] i_itemsWithData Список пар элементов и
				   ассоциированных с ними данных для для добавления в список
				   альтернатив.
				*/
				void
				    AddItemsWithData (const QList< QPair< QString, QVariant > >&
				                          i_items_with_data);

				void
				    SetItemsWithData (const QList< QPair< QString, QVariant > >&
				                          i_items_with_data);

				void SetComboBox (QComboBox* i_combo_box);

				void AttachToComboBox (QComboBox* i_combo_box);


			Q_SIGNALS:
				/**
				   @brief Отправка данных, ассоциированных с текущим выбранным
				   элементом списка.

				   @details Сигнал испускается только в случае, если
				   ассоциированные данные валидны.
				*/
				void CurrentItemData (QVariant);


			private:
				class Impl;
				utk::Pimpl< Impl > impl_;
			};
		} // namespace v1
	}     // namespace qt
} // namespace utk


Q_DECLARE_METATYPE (utk::qt::v1::ComboBoxExt*);


#endif /* INCLUDE_UTK_QT_WIDGET_COMBOBOXEXT_HPP */
