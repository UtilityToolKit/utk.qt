// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/include/utk/qt/qobject/QRectDesrializer.hpp
//
// Description: Declaration of the QRectDeserializer class.


#ifndef INCLUDE_UTK_QT_QOBJECT_QRECTDESERIALIZER_HPP
#define INCLUDE_UTK_QT_QOBJECT_QRECTDESERIALIZER_HPP


#include "utk/qt/qobject/ICustomDeserializer.hpp"


namespace utk {
	namespace qt {
		inline namespace v1 {
			/**
			  @brief Deserializer for QRectF type.
			*/
			class QRectFDeserializer : public ICustomDeserializer {
			public:
				static ICustomDeserializer::SharedPtr createObject ();

				static QString typeId ();

				virtual QVariant
				    deserialize (QDomNode i_serializedValue) override;
			};


			/**
			  @brief Deserializer for QRect type.
			*/
			class QRectDeserializer : public ICustomDeserializer {
			public:
				static ICustomDeserializer::SharedPtr createObject ();

				static QString typeId ();

				virtual QVariant
				    deserialize (QDomNode i_serializedValue) override;
			};
		}
	}
}


#endif /* INCLUDE_UTK_QT_QOBJECT_QRECTDESERIALIZER_HPP */
