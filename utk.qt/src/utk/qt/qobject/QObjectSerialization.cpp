// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/src/utk/qt/qobject/QObjectSerialization.cpp
//
// Description: Definition of the functions used to implement QObject
//              meta-information processing and QObject serialisation.


#include "utk/qt/qobject/QObjectSerialization.hpp"


#include <QObject>
#include <QVariant>

#include <QMetaObject>
#include <QMetaProperty>


#include "utk/qt/qobject/ICustomDeserializer.hpp"
#include "utk/qt/qobject/ICustomSerializer.hpp"


/*
   Namespace is defined to prevent name colisions.
*/
namespace {
	/**
	   @brief A helper function to get XML DOM node tag name for object.

	   @param [in] i_object Object to get XML DOM node name for.

	   @param [in] i_tagNameSource Source to get XML DOM node tag name.

	   @param [in] i_tagName Tag name to use is i_tagNameSource ==
	   TAG_USER_DEFINED.
	*/
	QString getTagNameForObject (
	    QObject* i_object,
	    utk::qt::TagNameSource i_tagNameSource,
	    QString i_tagName) {
		QString tagName;

		switch (i_tagNameSource) {
		case utk::qt::TagNameSource::TAG_TYPE_NAME: {
			tagName = i_object->metaObject ()->className ();

			break;
		}

		case utk::qt::TagNameSource::TAG_OBJECT_NAME: {
			tagName = i_object->objectName ();

			break;
		}

		case utk::qt::TagNameSource::TAG_USER_DEFINED: {
			tagName = i_tagName;

			break;
		}

		default: {
			/// @todo Implement error processing with exception.

			break;
		}
		}

		return tagName;
	}
}


QSet< QString > utk::qt::v1::objectPropertiesSet (QObject* i_object) {
	QSet< QString > objectPropertiesSet;

	const QMetaObject* metaObject (i_object->metaObject ());

	for (int propertyIndex (0), propertiesCount (metaObject->propertyCount ());
	     propertyIndex < propertiesCount;
	     ++propertyIndex) {
		QMetaProperty metaProperty (metaObject->property (propertyIndex));

		objectPropertiesSet.insert (metaProperty.name ());
	}

	return objectPropertiesSet;
}


QDomNode utk::qt::v1::serialize (
    QObject* i_object,
    utk::qt::TagNameSource i_tagNameSource,
    QString i_tagName,
    QStringList i_propertiesNames) {
	QDomDocument serializedDocument;

	QDomElement serializedElement (serializedDocument.createElement (
	    getTagNameForObject (i_object, i_tagNameSource, i_tagName)));

	serializedDocument.appendChild (serializedElement);

	QStringList objectPropertiesNames (
	    i_propertiesNames.isEmpty () ? objectPropertiesSet (i_object).values ()
	                                 : i_propertiesNames);

	foreach (QString propertyName, objectPropertiesNames) {
		QVariant propertyValue (i_object->property (propertyName.toLatin1 ()));

		if (propertyValue.canConvert (QVariant::String)) {
			serializedElement.setAttribute (
			    propertyName, propertyValue.toString ());
		}
		else {
			auto serializer = utk::qt::CustomSerializerFactory::createObject (
			    propertyValue.typeName ());

			if (serializer) {
				QDomElement propertyElement (
				    serializedDocument.createElement ("property"));

				propertyElement.setAttribute ("name", propertyName);

				propertyElement.appendChild (
				    serializer->serialize (propertyValue));

				serializedElement.appendChild (propertyElement);
			}
		}
	}

	return QDomNode (serializedDocument.documentElement ());
}


void utk::qt::v1::deserialize (QObject* o_object, QDomNode i_serialized) {
	QDomElement serializedElement (i_serialized.toElement ());

	if (!serializedElement.isNull ()) {
		QDomNamedNodeMap serializedAtributes (serializedElement.attributes ());

		for (int attributeIndex = 0;
		     attributeIndex < serializedAtributes.count ();
		     ++attributeIndex) {
			QDomAttr attribute (
			    serializedAtributes.item (attributeIndex).toAttr ());

			o_object->setProperty (
			    attribute.name ().toLatin1 (), attribute.value ());
		}

		for (QDomElement propertyElement =
		         serializedElement.firstChildElement ("property");
		     !propertyElement.isNull ();
		     propertyElement =
		         propertyElement.nextSiblingElement ("property")) {
			QString propertyName = propertyElement.attribute ("name");

			if (!propertyName.isEmpty ()) {
				QDomElement valueElement (
				    propertyElement.firstChild ().toElement ());

				if (!valueElement.isNull ()) {
					auto deserializer =
					    utk::qt::CustomDeserializerFactory::createObject (
					        valueElement.tagName ());

					if (deserializer) {
						QVariant propertyValue =
						    deserializer->deserialize (valueElement);

						if (propertyValue.isValid ()) {
							o_object->setProperty (
							    propertyName.toLatin1 (), propertyValue);
						}
					}
				}
			}
		}
	}
}


QDomNode utk::qt::v1::getSerializedObjectNode (
    QDomNode i_serialisedObjects,
    QObject* i_object,
    utk::qt::TagNameSource i_tagNameSource,
    QString i_tagName) {
	QDomNode serializedObjectNode;

	if ((nullptr != i_object) && !i_serialisedObjects.isNull ()) {
		QString serializedObjectTag (
		    getTagNameForObject (i_object, i_tagNameSource, i_tagName));

		if (serializedObjectTag ==
		    i_serialisedObjects.toElement ().tagName ()) {
			serializedObjectNode = i_serialisedObjects;
		}
		else {
			serializedObjectNode =
			    i_serialisedObjects.firstChildElement (serializedObjectTag);

			if (serializedObjectNode.isNull ()) {
				for (auto childNode = i_serialisedObjects.firstChild ();
				     !childNode.isNull () && serializedObjectNode.isNull ();
				     childNode = childNode.nextSibling ()) {
					serializedObjectNode = getSerializedObjectNode (
					    childNode, i_object, i_tagNameSource, i_tagName);
				}
			}
		}
	}

	return serializedObjectNode;
}
