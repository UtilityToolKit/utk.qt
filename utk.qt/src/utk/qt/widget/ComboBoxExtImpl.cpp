// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/src/utk/qt/widget/ComboBoxExtImpl.cpp
//
// Description: Definition of the ComboBoxExt PIMPL class.


#include "ComboBoxExtImpl.hpp"

#include <utk/exception/LogicError/InvalidLogicArgument.hpp>


utk::qt::v1::ComboBoxExt::Impl::Impl (utk::qt::v1::ComboBoxExt* i_owner)
    : owner_{i_owner} {
	if (!i_owner) {
		BOOST_THROW_EXCEPTION (
		    utk::exception::InvalidLogicArgument ()
		    << utk::exception::InvalidLogicArgument::ArgumentName ("i_owner")
		    << utk::exception::InvalidLogicArgument::Description (
		           "Owner cannot be nullptr"));
	}
}


void utk::qt::v1::ComboBoxExt::Impl::AddItemsToComboBox (
    const QList< QPair< QString, QVariant > >& i_items_with_data) {
	if (&items_with_data != &i_items_with_data) {
		items_with_data.reserve (
		    items_with_data.size () + i_items_with_data.size ());

		items_with_data.append (i_items_with_data);
	}

	if (combo_box) {
		auto const notification_enabled{this->notification_enabled};

		owner_->DisableNotification ();

		for (auto const& item_with_data : i_items_with_data) {
			combo_box->addItem (item_with_data.first, item_with_data.second);
		}

		if (notification_enabled) {
			owner_->EnableNotification ();
		}
	}
}


void utk::qt::v1::ComboBoxExt::Impl::OnCurrentItemIndexChanged (
    int i_new_index) {
	QVariant current_item_data (combo_box->itemData (i_new_index));

	if (current_item_data.isValid ()) {
		Q_EMIT owner_->CurrentItemData (current_item_data);
	}
}
