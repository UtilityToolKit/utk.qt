// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/src/utk/qt/qobject/QRectDesrializer.cpp
//
// Description: Definition of the QRectDeserializer class.


#include "utk/qt/qobject/QRectDeserializer.hpp"


#include <QRect>
#include <QRectF>


utk::factory::AddProduction<
    utk::qt::v1::ICustomDeserializer,
    utk::qt::v1::QRectFDeserializer >
    addQRectFDeserializer;
utk::factory::AddProduction<
    utk::qt::v1::ICustomDeserializer,
    utk::qt::v1::QRectDeserializer >
    addQRectDeserializer;


utk::qt::v1::ICustomDeserializer::SharedPtr
    utk::qt::v1::QRectFDeserializer::createObject () {
	return std::make_shared< QRectFDeserializer > ();
}


QString utk::qt::v1::QRectFDeserializer::typeId () {
	static QString s_typeId (QMetaType::typeName (QMetaType::QRectF));

	return s_typeId;
}


QVariant
    utk::qt::v1::QRectFDeserializer::deserialize (QDomNode i_serializedValue) {
	QVariant deserializedValue;

	QDomElement serializedElement (i_serializedValue.toElement ());

	if (!serializedElement.isNull () &&
	    (typeId () == serializedElement.tagName ())) {
		deserializedValue = QRectF (
		    serializedElement.attribute ("x").toFloat (),
		    serializedElement.attribute ("y").toFloat (),
		    serializedElement.attribute ("width").toFloat (),
		    serializedElement.attribute ("height").toFloat ());
	}

	return deserializedValue;
}


utk::qt::v1::ICustomDeserializer::SharedPtr
    utk::qt::v1::QRectDeserializer::createObject () {
	return std::make_shared< QRectDeserializer > ();
}


QString utk::qt::v1::QRectDeserializer::typeId () {
	static QString s_typeId (QMetaType::typeName (QMetaType::QRect));

	return s_typeId;
}


QVariant
    utk::qt::v1::QRectDeserializer::deserialize (QDomNode i_serializedValue) {
	QDomElement serializedElement (i_serializedValue.toElement ());

	serializedElement.setTagName (QRectFDeserializer::typeId ());

	return QRectFDeserializer ().deserialize (serializedElement);
}
