// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/src/utk/qt/xml/xml_dom.cpp
//
// Description: Definition of the functions for working with QDomNode objects.


#include <QFile>
#include <QTextStream>

#include <utk/qt/file/isPossibleToReadFile.hpp>
#include <utk/qt/file/isPossibleToWriteFile.hpp>

#include <utk/qt/xml/xml_dom.hpp>


bool utk::qt::readXmlFile (QString i_fullFileName, QDomNode& o_content) {
	bool status (false);

	if (isPossibleToReadFile (i_fullFileName)) {
		QFile inputFile (i_fullFileName);
		QDomDocument inputDocument;

		if (inputDocument.setContent (&inputFile, false)) {
			o_content = inputDocument;

			status = !o_content.isNull ();
		}
	}

	return status;
}


bool utk::qt::writeXmlFile (
    QString i_fullFileName, QDomNode i_content, QString i_codec) {
	bool status (false);

	if (isPossibleToWriteFile (i_fullFileName, true, true)) {
		QFile outputFile (i_fullFileName);

		if (outputFile.open (QIODevice::WriteOnly | QIODevice::Text)) {
			QTextStream outputStream (&outputFile);

			outputStream.setCodec (i_codec.toLatin1 ());

			i_content.save (outputStream, 4, QDomNode::EncodingFromTextStream);

			status = true;
		}
	}

	return status;
}
