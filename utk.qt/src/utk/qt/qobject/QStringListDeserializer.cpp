// Copyright 2015-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.qt/utk.qt/src/utk/qt/qobject/QStringListDeserializer.cpp
//
// Description: Definition of the QStringListDeserializer class.


#include "utk/qt/qobject/QStringListDeserializer.hpp"


#include <QStringList>


utk::factory::AddProduction<
    utk::qt::v1::ICustomDeserializer,
    utk::qt::v1::QStringListDeserializer >
    addQStringListDeserializer;


utk::qt::v1::ICustomDeserializer::SharedPtr
    utk::qt::v1::QStringListDeserializer::createObject () {
	return std::make_shared< QStringListDeserializer > ();
}


QString utk::qt::v1::QStringListDeserializer::typeId () {
	static QString s_typeId (QMetaType::typeName (QMetaType::QStringList));

	return s_typeId;
}

QVariant utk::qt::v1::QStringListDeserializer::deserialize (
    QDomNode i_serializedValue) {
	QVariant deserializedValue;

	QDomElement serializedElement (i_serializedValue.toElement ());

	if (!serializedElement.isNull () &&
	    (typeId () == serializedElement.tagName ())) {
		QStringList stringList;

		for (QDomElement stringElement =
		         serializedElement.firstChildElement ("string");
		     !stringElement.isNull ();
		     stringElement = stringElement.nextSiblingElement ("string")) {
			stringList.append (stringElement.attribute ("text"));
		}

		deserializedValue = stringList;
	}

	return deserializedValue;
}
